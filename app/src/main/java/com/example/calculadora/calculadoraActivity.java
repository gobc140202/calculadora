package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class calculadoraActivity extends AppCompatActivity {

    private Button limpiar, regresar, suma, resta, division, multiplicacion;
    private EditText etNum1, etNum2;
    private TextView txtResultado;

public void iniciarObjetos(){
    etNum1 = (EditText) findViewById(R.id.etNum1);
    etNum2 = (EditText) findViewById(R.id.etNum2);
    txtResultado = (TextView) findViewById(R.id.txtResultado);
    suma = (Button) findViewById(R.id.btnSuma);
    resta = (Button) findViewById(R.id.btnResta);
    multiplicacion = (Button) findViewById(R.id.btnMultiplicacion);
    division = (Button) findViewById(R.id.btnDivision);
    limpiar = (Button) findViewById(R.id.btnLimpiar);
    regresar = (Button) findViewById(R.id.btnRegresar);
}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarObjetos();
        suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etNum1.getText().toString().isEmpty() || etNum2.getText().toString().isEmpty()) {
                    Toast.makeText(calculadoraActivity.this, "No se capturó la información necesaria", Toast.LENGTH_SHORT).show();
                } else {

                    float num1=Float.parseFloat(etNum1.getText().toString());
                    float num2=Float.parseFloat(etNum2.getText().toString());
                    calculadoraFunciones calculadora = new calculadoraFunciones(num1, num2);
                    float resultado = calculadora.suma();
                    txtResultado.setText(""+resultado);
                }
            }
        });

        resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etNum1.getText().toString().isEmpty() || etNum2.getText().toString().isEmpty()) {
                    Toast.makeText(calculadoraActivity.this, "No se capturó la información necesaria", Toast.LENGTH_SHORT).show();
                } else {
                    float num1=Float.parseFloat(etNum1.getText().toString());
                    float num2=Float.parseFloat(etNum2.getText().toString());
                    calculadoraFunciones calculadora = new calculadoraFunciones(num1, num2);
                    float resultado = calculadora.resta();
                    txtResultado.setText(""+resultado);
                }
            }
        });

        multiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etNum1.getText().toString().isEmpty() || etNum2.getText().toString().isEmpty()) {
                    Toast.makeText(calculadoraActivity.this, "No se capturó la información necesaria", Toast.LENGTH_SHORT).show();
                } else {
                    float num1=Float.parseFloat(etNum1.getText().toString());
                    float num2=Float.parseFloat(etNum2.getText().toString());
                    calculadoraFunciones calculadora = new calculadoraFunciones(num1, num2);
                    float resultado = calculadora.multiplicacion();
                    txtResultado.setText(""+resultado);
                }
            }
        });

        division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etNum1.getText().toString().isEmpty() || etNum2.getText().toString().isEmpty()) {
                    Toast.makeText(calculadoraActivity.this, "No se capturó la información necesaria", Toast.LENGTH_SHORT).show();
                }
                else if(etNum2.getText().toString().equals("0")){
                    Toast.makeText(calculadoraActivity.this, "No se puede dividir entre cero", Toast.LENGTH_SHORT).show();
                }
                else{
                    float num1=Float.parseFloat(etNum1.getText().toString());
                    float num2=Float.parseFloat(etNum2.getText().toString());
                    calculadoraFunciones calculadora = new calculadoraFunciones(num1, num2);
                    float resultado = calculadora.division();
                    txtResultado.setText(""+resultado);
                }
            }
        });


        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNum1.setText("");
                etNum2.setText("");
                txtResultado.setText("");
            }
        });

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(calculadoraActivity.this, loginActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}









