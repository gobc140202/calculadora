package com.example.calculadora;

import java.io.Serializable;

public class calculadoraFunciones implements Serializable {

    private float num1;
    private float num2;

    public calculadoraFunciones(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    public float suma(){
        float resultado=0;
        resultado=this.num1+this.num2;
        return resultado;
    }
    public float resta(){
        float resultado=0;
        resultado=this.num1-this.num2;
        return resultado;
    }
    public float multiplicacion(){
        float resultado=0;
        resultado=this.num1*this.num2;
        return resultado;
    }

    public float division(){
        float resultado=0;
        resultado=this.num1/this.num2;
        return resultado;
    }
}
