package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class loginActivity extends AppCompatActivity {

    private EditText usuario, contrasena;
    private Button ingresar, cerrar;

    public void iniciarObjetos(){
        usuario = findViewById(R.id.etUsuario);
        contrasena = findViewById(R.id.etContrasena);
        ingresar = findViewById(R.id.btnIngresar);
        cerrar = findViewById(R.id.btnCerrar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarObjetos();
        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (usuario.getText().toString().isEmpty() || contrasena.getText().toString().isEmpty()) {
                    Toast.makeText(loginActivity.this, "No se capturo la informacion necesaria", Toast.LENGTH_SHORT).show();
                } else if (usuario.getText().toString().equals(getString(R.string.usuario))||contrasena.getText().toString().equals(getString(R.string.contrasena))){
                        usuario.setText("");
                        contrasena.setText("");
                        Intent intent = new Intent(loginActivity.this, calculadoraActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(loginActivity.this, "Credenciales incorrectas", Toast.LENGTH_SHORT).show();
                    }
                }
        });

        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }
}
